import 'package:flutter/material.dart';
import 'package:flutter_first/page/add_page.dart';
import 'package:flutter_first/page/main_page.dart';
import 'package:flutter_first/page/home_page.dart';
import 'package:flutter_first/page/users_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Kanit',
        scaffoldBackgroundColor: Colors.white70,
        accentColor: Colors.amber,
        primarySwatch: Colors.pink,
      ),
      home: HomePage(),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => HomePage(),
        '/users': (BuildContext context) => UsersPage(),
      },
    );
  }
}

