import 'package:flutter/material.dart';
import 'package:flutter_first/page/page_one.dart';
import 'package:flutter_first/page/page_two.dart';
import 'package:flutter_first/page/page_three.dart';

import 'add_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;
  List pages = [PageOne(), PageTwo(), PageThree()];

  TextStyle myStyle = TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    Widget appBar = AppBar(
      title: Text('Home page', style: myStyle),
      backgroundColor: Colors.pink,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            Navigator.of(context).pushNamed('/users');
          },
        )
      ],
    );

    Widget floatingAction = FloatingActionButton(
        backgroundColor: Colors.pink,
        onPressed: () async {
          var res = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddPage('hello flutter')));
          print(res);
        },
        child: Icon(Icons.add));

    Widget bottomNavBar = BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: (int index) {
        setState(() {
          currentIndex = index;
        });
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('หน้าแรก')),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_circle), title: Text('ข้อมูลส่วนตัว')),
        BottomNavigationBarItem(
            icon: Icon(Icons.settings), title: Text('ตั้งค่า'))
      ],
    );

    Widget drawers = Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: const <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Drawer Header',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.change_history),
            title: Text('Change history'),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: pages[currentIndex],
      drawer: drawers,
      floatingActionButton: floatingAction,
      bottomNavigationBar: bottomNavBar,
    );
  }
}
