import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  String firstName = 'ekkachai';
  String lastName = 'fiest';

  void testMethod(String name){
    print(name);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('main page')),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            testMethod(firstName);
          },
          color: Colors.pink,
          textColor: Colors.white,
          child: Text('Click me!'),
        )
      ),
    );
  }
}
