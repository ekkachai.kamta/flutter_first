import 'package:flutter/material.dart';

class PageThree extends StatefulWidget {
  @override
  _PageThreeState createState() => _PageThreeState();
}

class _PageThreeState extends State<PageThree> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 10.0,bottom: 10.0),
                child: Row(
                 children: <Widget>[
                   Expanded(
                     child: Icon(Icons.edit),
                   ),
                   Expanded(
                     flex: 5,
                     child: Text(
                       'เมนูตั้งค่าข้อมูลส่วนตัว',
                       style: TextStyle(fontSize: 18.0),
                     ),
                   )
                 ],
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.account_circle),
                title: Text('ข้อมูลส่วนตัว'),
                subtitle: Text('จัดการข้อมูลส่วนของผู้ใช้'),
                trailing: Icon(Icons.keyboard_arrow_right),
              ),
              ListTile(
                leading: Icon(Icons.vpn_key),
                title: Text('เปลี่ยนรหัสผ่าน'),
                subtitle: Text('เปลี่ยนรหัสผ่านการเข้าใข้งานแอพ'),
                trailing: Icon(Icons.keyboard_arrow_right),
              ),
            ],
          ),
        )
      ],
    );
  }
}
