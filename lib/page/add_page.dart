import 'package:flutter/material.dart';

class AddPage extends StatefulWidget {
  var params;

  AddPage(this.params);

  @override
  _AddPageState createState() => _AddPageState(params);
}

class _AddPageState extends State<AddPage> {
  var params;

  _AddPageState(this.params);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add page'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () => Navigator.of(context).pop('Ok'),
          child: Text(params),
        ),
      ),
    );
  }
}
