import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class UsersPage extends StatefulWidget {
  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  var users;
  bool isLoading = true;

  Future<Null> fetchUsers() async {

    final response = await http.get('https://randomuser.me/api/?result=10');

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response, then parse the JSON.
//      return Album.fromJson(json.decode(response.body));
      var jsonResponse = json.decode(response.body);
      print(jsonResponse);
      setState(() {
        isLoading = false;
        users = jsonResponse['results'];
      });
    } else {
      // If the server did not return a 200 OK response, then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Uset List')),
      body: RefreshIndicator(
        onRefresh: fetchUsers,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
            itemBuilder: (context, int index) {
              return Card(
                child: ListTile(
                  onTap: () {},
                  title: Text('${users[index]['gender']}'),
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              );
            },
            itemCount: users != null ? users.length : 0
        ),
      ),
    );
  }
}
