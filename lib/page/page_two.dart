import 'package:flutter/material.dart';

class PageTwo extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          height: 100.0,
          color: Colors.pink,
        ),
        Container(
          height: 100.0,
          color: Colors.amber,
        ),
        Container(
          height: 100.0,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 100.0,
                  color: Colors.amberAccent,
                ),
              ),
              Expanded(
                child: Container(
                  height: 100.0,
                  color: Colors.white70,
                ),
              ),
              Expanded(
                child: Container(
                  height: 100.0,
                  color: Colors.red,
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
